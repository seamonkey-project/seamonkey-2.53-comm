--- CalDavSession.jsm
+++ CalDavSession.jsm
@@ -25,23 +25,22 @@ class CalDavGoogleOAuth extends OAuth2 {
   /**
    * Constructs a new Google OAuth authentication provider
    *
    * @param {String} sessionId    The session id, used in the password manager
    * @param {String} name         The user-readable description of this session
    */
   constructor(sessionId, name) {
     /* eslint-disable no-undef */
-    super(
-      "https://accounts.google.com/o/oauth2/auth",
-      "https://www.googleapis.com/oauth2/v3/token",
-      "https://www.googleapis.com/auth/calendar",
-      OAUTH_CLIENT_ID,
-      OAUTH_HASH
-    );
+    super("https://www.googleapis.com/auth/calendar", {
+      authorizationEndpoint: "https://accounts.google.com/o/oauth2/auth",
+      tokenEndpoint: "https://www.googleapis.com/oauth2/v3/token",
+      clientId: OAUTH_CLIENT_ID,
+      clientSecret: OAUTH_HASH,
+    });
     /*  eslint-enable no-undef */
 
     this.id = sessionId;
     this.origin = "oauth:" + sessionId;
     this.pwMgrId = "Google CalDAV v2";
 
     this._maybeUpgrade(name);
 
@@ -56,19 +55,19 @@ class CalDavGoogleOAuth extends OAuth2 {
 
   /**
    * If no token is found for "Google CalDAV v2", this is either a new session (in which case
    * it should use Thunderbird's credentials) or it's already using Thunderbird's credentials.
    * Detect those situations and switch credentials if necessary.
    */
   _maybeUpgrade() {
     if (!this.refreshToken) {
-      [this.clientId, this.consumerSecret] = OAuth2Providers.getIssuerDetails(
-        "accounts.google.com"
-      );
+      const issuerDetails = OAuth2Providers.getIssuerDetails("accounts.google.com");
+      this.clientId = issuerDetails.clientId;
+      this.consumerSecret = issuerDetails.clientSecret;
       this.origin = "oauth://accounts.google.com";
       this.pwMgrId = "https://www.googleapis.com/auth/calendar";
     }
   }
 
   /**
    * Returns true if the token has expired, or will expire within the grace time.
    */
@@ -262,17 +261,19 @@ class CalDavTestOAuth extends CalDavGoog
     // I don't know why, but tests refuse to work with a plain HTTP endpoint
     // (the request is redirected to HTTPS, which we're not listening to).
     // Just use an HTTPS endpoint.
     this.redirectionEndpoint = "https://localhost";
   }
 
   _maybeUpgrade() {
     if (!this.refreshToken) {
-      [this.clientId, this.consumerSecret] = OAuth2Providers.getIssuerDetails("mochi.test");
+      const issuerDetails = OAuth2Providers.getIssuerDetails("mochi.test");
+      this.clientId = issuerDetails.clientId;
+      this.consumerSecret = issuerDetails.clientSecret;
       this.origin = "oauth://mochi.test";
       this.pwMgrId = "test_scope";
     }
   }
 }
 
 /**
  * A session for the caldav provider. Two or more calendars can share a session if they have the
