/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
ChromeUtils.import("resource://gre/modules/Services.jsm");

/**
 * A startup/shutdown observer, triggers init()/uninit() methods.
 * @constructor
 */
function InspectorStartup() {}
InspectorStartup.prototype = {
  classDescription: "DOMi Startup",
  contractID: "@mozilla.org/domi/startup;1",
  classID: Components.ID("{4788f52a-0fbb-494c-8ffd-25e0ff39d80d}"),
  _xpcom_categories: [{ category: "app-startup", service: true }],

  QueryInterface: XPCOMUtils.generateQI([Ci.nsISupportsWeakReference]),

  observe: function(subject, topic, data) {
    switch (topic) {
      case "app-startup":
        Services.obs.addObserver(this, "profile-after-change", true);
        break;
      case "profile-after-change":
        if (Services.prefs.getBoolPref("inspector.inspectElementKeyCombo.enabled", false)) {
          Services.obs.addObserver(this, "quit-application", true);
          ChromeUtils.import("resource://inspector/InspectElement.jsm");
          InspectElement.init();
        }
        break;
      case "quit-application":
        Services.obs.removeObserver(this, "quit-application");
        InspectElement.uninit();
        break;
    }
  }
};

var NSGetFactory = XPCOMUtils.generateNSGetFactory([InspectorStartup]);
