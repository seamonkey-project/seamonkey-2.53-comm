/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

var EXPORTED_SYMBOLS = ["InspectElement"];

ChromeUtils.import("resource://gre/modules/Services.jsm");

var InspectElement = {
  handleEvent: function(e) {
    // Shift + right click.
    if (!e.shiftKey || e.button != 2) {
      return;
    }

    try {
      e.stopPropagation();
      e.preventDefault();
    } catch (ex) {}

    if (e.type != "click") {
      return;
    }

    let elem = e.originalTarget;
    let shadowElem = e.target;
    let win = e.currentTarget;
    this.inspect(win, elem, shadowElem);
  },
  inspect: function(win, elem, shadowElem) {
    win.openDialog("chrome://inspector/content/", "_blank",
                   "chrome, all, dialog=no", elem);
    this.closePopup(elem, win);
  },
  closePopup: function (elem, win) {
    var parent = elem.parentNode;
    var list = [];

    while (parent != win && parent != null) {
      if (parent.localName == "menupopup" || parent.localName == "popup") {
        list.push(parent);
      }
      parent = parent.parentNode;
    }

    var len = list.length;

    if (!len) {
      return;
    }

    list[len - 1].hidePopup();
  },
  aListener: {
    onOpenWindow: function (aWindow) {
      var win = aWindow.docShell.QueryInterface(Ci.nsIInterfaceRequestor)
                                .getInterface(Ci.nsIDOMWindow);

      win.addEventListener("load", function _inspect() {
        this.removeEventListener("load", _inspect, false);
        win.addEventListener("click", InspectElement, true);

        if (Services.appinfo.OS == "WINNT") {
          return;
        }

        win.addEventListener("mouseup", InspectElement, false);
        win.addEventListener("contextmenu", InspectElement, true);
      }, false);
    },
    onCloseWindow: function (aWindow) {},
    onWindowTitleChange: function (aWindow, aTitle) {},
  },
  init: function() {
     Services.wm.addListener(this.aListener);
    var cw =  Services.ww.getWindowEnumerator();

    while (cw.hasMoreElements()) {
      var win = cw.getNext().QueryInterface(Ci.nsIDOMWindow);
      win.addEventListener("click", InspectElement, true);

      if (Services.appinfo.OS == "WINNT") {
        continue;
      }

      win.addEventListener("mouseup", InspectElement, false);
      win.addEventListener("contextmenu", InspectElement, true);
    }
  },
  uninit: function() {
    Services.wm.removeListener(this.aListener);
    var cw =  Services.ww.getWindowEnumerator();

    while (cw.hasMoreElements()) {
      var win = cw.getNext().QueryInterface(Ci.nsIDOMWindow);
      win.removeEventListener("click", InspectElement, true);

      if (Services.appinfo.OS == "WINNT") {
        continue;
      }

      win.removeEventListener("mouseup", InspectElement, false);
      win.removeEventListener("contextmenu", InspectElement, true);
    }
  }
}