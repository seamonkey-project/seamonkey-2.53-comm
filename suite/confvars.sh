#! /bin/sh
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

MOZ_APP_VENDOR=Mozilla
MOZ_APP_NAME=seamonkey
MOZ_APP_DISPLAYNAME=SeaMonkey

MOZ_APP_VERSION=$SEAMONKEY_VERSION
MOZ_APP_VERSION_DISPLAY=$SEAMONKEY_VERSION_DISPLAY
MOZ_PKG_VERSION=$SEAMONKEY_VERSION_PACKAGE

BROWSER_CHROME_URL=chrome://navigator/content/navigator.xul

MOZ_BRANDING_DIRECTORY=comm/suite/branding/seamonkey
MOZ_OFFICIAL_BRANDING_DIRECTORY=comm/suite/branding/seamonkey

MOZ_UPDATER=1

MOZ_APP_ID={92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}
MOZ_PROFILE_MIGRATOR=1

# Include the DevTools client, not just the server (which is the default)
MOZ_DEVTOOLS=all

NSS_EXTRA_SYMBOLS_FILE=../comm/mailnews/nss-extra.symbols
