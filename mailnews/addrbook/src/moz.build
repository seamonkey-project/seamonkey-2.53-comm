# vim: set filetype=python:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

EXPORTS += [
    'nsAbDirProperty.h',
    'nsDirPrefs.h',
    'nsVCardObj.h',
]

SOURCES += [
    'nsAbAddressCollector.cpp',
    'nsAbBooleanExpression.cpp',
    'nsAbBoolExprToLDAPFilter.cpp',
    'nsAbBSDirectory.cpp',
    'nsAbCardProperty.cpp',
    'nsAbContentHandler.cpp',
    'nsAbDirectoryQuery.cpp',
    'nsAbDirectoryQueryProxy.cpp',
    'nsAbDirFactoryService.cpp',
    'nsAbDirProperty.cpp',
    'nsAbLDAPDirectory.cpp',
    'nsAbLDAPDirectoryQuery.cpp',
    'nsAbLDAPDirFactory.cpp',
    'nsAbLDAPListenerBase.cpp',
    'nsAbLDAPReplicationData.cpp',
    'nsAbLDAPReplicationQuery.cpp',
    'nsAbLDAPReplicationService.cpp',
    'nsAbLDIFService.cpp',
    'nsAbManager.cpp',
    'nsAbMDBCard.cpp',
    'nsAbMDBDirectory.cpp',
    'nsAbMDBDirFactory.cpp',
    'nsAbMDBDirProperty.cpp',
    'nsAbQueryStringToExpression.cpp',
    'nsAbView.cpp',
    'nsAddbookProtocolHandler.cpp',
    'nsAddbookUrl.cpp',
    'nsAddrDatabase.cpp',
    'nsDirPrefs.cpp',
    'nsMsgVCardService.cpp',
    'nsVCard.cpp',
    'nsVCardObj.cpp',
]

if CONFIG['OS_ARCH'] == 'WINNT' and CONFIG['MOZ_MAPI_SUPPORT']:
    SOURCES += [
        'nsAbOutlookDirectory.cpp',
        'nsAbOutlookDirFactory.cpp',
        'nsAbWinHelper.cpp',
        'nsMapiAddressBook.cpp',
        'nsWabAddressBook.cpp',
    ]
    LOCAL_INCLUDES += ["/comm/mailnews/mapi/include"]

if CONFIG['OS_ARCH'] == 'Darwin':
    SOURCES += [
        'nsAbOSXDirFactory.cpp',
    ]

    SOURCES += [
        'nsAbOSXCard.mm',
        'nsAbOSXDirectory.mm',
        'nsAbOSXUtils.mm',
    ]

EXTRA_COMPONENTS += [
    'nsAbAutoCompleteMyDomain.js',
    'nsAbAutoCompleteSearch.js',
    'nsAbLDAPAttributeMap.js',
    'nsAbLDAPAutoCompleteSearch.js',
    'nsAddrbook.manifest',
]

FINAL_LIBRARY = 'mail'
